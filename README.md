# Instrukcja
Zadanie polega na stworzeniu komponentów oraz struktury danych (store lub context), w opraciu o pliki:
- `./build/index.html`
- `./src/index.tsx`

## Instalacja
1. Uruchom komendę `yarn install`
2. Następnie `yarn build`
3. Folder `./build/` utworzy się automatycznie

## Kryteria akceptacji
1. Rozbij aplikację na komponenty.
2. Dane w formularzu powinny być wyświetlanie z wybranej struktury danych oraz do niej zapisywane.
3. Pola formularza powinny być zabezpieczone przed code injection
4. Napisz testy do komponentów.
5. Całość projektu powinna być instalowana komendą `yarn install`.
6. Kod powinien wygenerować się bez błędów, po wywołaniu komendy `yarn build:prod`