import * as React from 'react';
import * as ReactDOM from 'react-dom';

const index = require('file-loader?name=[name].[ext]!../index.html');

const Form = () => {
    return (
        <form>
            <label htmlFor={'input'}>Input:</label>
            <input id={'input'} value={'Wartość początkowa'}/>
            <label htmlFor={'textarea'}>Textarea:</label>
            <textarea id={'textarea'} value={'Wartość\npoczątkowa'}/>
        </form>
    );
};

ReactDOM.render(<Form />, document.getElementById('root'));
